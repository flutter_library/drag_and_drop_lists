import 'package:example/basic_example.dart';
import 'package:example/drag_handle_example.dart';
import 'package:example/drag_into_list_example.dart';
import 'package:example/expansion_tile_example.dart';
import 'package:example/fixed_example.dart';
import 'package:example/horizontal_example.dart';
import 'package:example/list_tile_example.dart';
import 'package:example/sliver_example.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Drag and Drop Lists',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => BasicExample(),
        '/list_tile_example': (context) => ListTileExample(),
        '/expansion_tile_example': (context) => ExpansionTileExample(),
        '/sliver_example': (context) => SliverExample(),
        '/horizontal_example': (context) => HorizontalExample(),
        '/drag_into_list_example': (context) => DragIntoListExample(),
        '/fixed_example': (context) => FixedExample(),
        '/drag_handle_example': (context) => DragHandleExample(),
      },
    );
  }
}

// BUG
// import 'package:flutter/material.dart';
//
// void main() => runApp(MyApp());
//
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) => MaterialApp(
//         home: HomePage(),
//       );
// }
//
// class HomePage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     // This method is rerun every time setState is called, for instance as done
//     // by the _incrementCounter method above.
//     //
//     // The Flutter framework has been optimized to make rerunning build methods
//     // fast, so that you can just rebuild anything that needs updating rather
//     // than having to individually change instances of widgets.
//     return Scaffold(
//       appBar: AppBar(
//         // Here we take the value from the MyHomePage object that was created by
//         // the App.build method, and use it to set our appbar title.
//         title: Text('bug'),
//       ),
//       body: ListView(
//         children: [
//           LongPressDraggable(
//             feedback: Material(
//               child: Container(
//                 height: 20,
//                 width: 20,
//                 color: Colors.green,
//               ),
//             ),
//             child: ListTile(
//               title: Text('Click'),
//               onTap: () {
//                 print('tapped');
//               },
//             ),
//             childWhenDragging: Container(
//               height: 20,
//               color: Colors.pinkAccent,
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
