import 'package:drag_and_drop_lists/drag_and_drop_interface.dart';
import 'package:flutter/widgets.dart';

class DragAndDropItem implements DragAndDropInterface {
  /// The child widget of this item.
  Widget child;
  num currentIndex;

  /// Whether or not this item can be dragged.
  /// Set to true if it can be reordered.
  /// Set to false if it must remain fixed.
  final bool canDrag;
  String type;
  final num listindex;
  final num index;
  final items;
  String title;
  num layout;
  num countItem;
  num row_postion;
  num itemIndex;
  String upperIndex;
  bool addSameRow;
  bool addlastitem;
  String first_title;
  Map first_item;
  Map last_item;
  String last_title;
  List<Map> field;

  DragAndDropItem(
      {@required this.child,
      this.canDrag = true,
      this.type,
      this.listindex,
      this.index,
      this.items,
      this.layout,
      this.countItem,
      this.row_postion,
      this.last_title,
      this.first_title,
      this.addSameRow,
      this.upperIndex,
      this.itemIndex,
      this.first_item,
      this.last_item,
      this.addlastitem,
      this.title,
      this.field,
      this.currentIndex});
}
