import 'package:drag_and_drop_lists/drag_and_drop_builder_parameters.dart';
import 'package:drag_and_drop_lists/drag_and_drop_list_interface.dart';
import 'package:drag_and_drop_lists/drag_and_drop_lists.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DragAndDropItemTarget extends StatefulWidget {
  final Widget child;
  final DragAndDropListInterface parent;
  final DragAndDropBuilderParameters parameters;
  final OnItemDropOnLastTarget onReorderOrAdd;
  final int totalItem;
  final int layout;
  final bool hideController;
  bool enable;
  DragAndDropItemTarget(
      {@required this.child,
      @required this.onReorderOrAdd,
      @required this.parameters,
      this.totalItem,
      this.hideController = false,
      this.parent,
      this.layout,
      this.enable = true,
      Key key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _DragAndDropItemTarget();
}

class _DragAndDropItemTarget extends State<DragAndDropItemTarget>
    with TickerProviderStateMixin {
  DragAndDropItem _hoveredDraggable;
  bool onHover = false;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          crossAxisAlignment: widget.parameters.verticalAlignment,
          children: <Widget>[
            AnimatedSize(
              duration: Duration(
                  milliseconds: widget.parameters.itemSizeAnimationDuration),
              vsync: this,
              alignment: Alignment.bottomCenter,
              child: _hoveredDraggable != null
                  ? Opacity(
                      opacity: widget.parameters.itemGhostOpacity,
                      child: widget.parameters.itemGhost ??
                          _hoveredDraggable.child,
                    )
                  : Container(),
            ),
            widget.child ??
                Container(
                  height: 20,
                ),
          ],
        ),
        if (!widget.hideController)
          Positioned.fill(
            child: DragTarget<DragAndDropItem>(
              builder: (context, candidateData, rejectedData) {
                if (candidateData != null && candidateData.isNotEmpty) {}
                return Container(
                  color: onHover
                      ? Colors.blue
                      : widget.layout == 2
                          ? Colors.green
                          : Colors.deepOrangeAccent,
                  child: Center(
                    child: Text(
                      'Drop to add lastest item.',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                );
              },
              onWillAccept: (incoming) {
                print('onWillAccept');
                print(widget.layout);
                incoming.itemIndex = widget.totalItem;
                print(incoming.itemIndex);
                bool accept = true;
                if (widget.parameters.itemTargetOnWillAccept != null)
                  accept = widget.parameters
                      .itemTargetOnWillAccept(incoming, widget);
                if (accept && mounted) {
                  setState(() {
                    onHover = true;
                    _hoveredDraggable = incoming;
                  });
                }
                return accept;
              },
              onLeave: (incoming) {
                print('onLeave');
                if (mounted) {
                  setState(() {
                    onHover = false;
                    _hoveredDraggable = null;
                  });
                }
              },
              onAccept: (incoming) {
                print('onAccept');
                if (mounted) {
                  incoming.addlastitem = true;
                  setState(() {
                    if (widget.onReorderOrAdd != null)
                      widget.onReorderOrAdd(incoming, widget.parent, widget);
                    _hoveredDraggable = null;
                    onHover = false;
                  });
                }
              },
            ),
          ),
      ],
    );
  }
}
